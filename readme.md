# Raspi minimal SSH Motd Arch Linux

- Create your SSH Motd or SSH Banner

## Requirements

- Raspberry Pi 3/4 (tested)
- Arch Linux OS
- [Video Tutorial](https://youtu.be/DWqLwvqOJ_A)

## Introductions

- step 1 | update system
```bash
sudo pacman -Syu
```
- step 2 | install dependencies
```bash
sudo pacman -S pacman-contrib git
```
- step 3 | disable print motd + print last log
```bash
sudo nano /etc/ssh/sshd_config

PrintMotd no
PrintLastLog no
```
- step 4 | disable default session motd
```bash
sudo nano /etc/pam.d/system-login

#session    optional   pam_motd.so
```
- step 5 | download git repo
```bash
git clone https://gitlab.com/useredition-dev/raspi-minimal-motd-arch-linux.git
```
- step 6 | move files to the following places
```bash
cd raspi-minimal-motd-arch-linux

sudo mv mymotd.sh /etc/profile.d
sudo chmod +x /etc/profile.d/mymotd.sh
```
- step 7 | restart ssh daemon
```bash
sudo systemctl restart sshd
#or
exit
```