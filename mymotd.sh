#!/bin/bash
# create by youtube@UserEdition


#color
w="\033[0;37m"
b="\033[0;34m"

#info
hostname=`uname -n`
user=`whoami`
kernel=`uname -r`
cpu=`awk -F '[ :][ :]+' '/^model name/ {print $2; exit;}' /proc/cpuinfo`
cputemp=`cat /sys/class/thermal/thermal_zone0/temp`
uptime=`cat /proc/uptime | cut -f1 -d.`
updays=$((uptime/60/60/24))
uphours=$((uptime/60/60%24))
upmins=$((uptime/60%60))
upsec=$((uptime%60))
memory1=`free -t -m | grep "Mem" | awk '{print $3" MB";}'`
memory2=`free -t -m | grep "Mem" | awk '{print $2" MB";}'`
memory3=`free -t -m | grep "Mem" | awk '{print $7" MB";}'`
pacman=`checkupdates | wc -l`

#clear terminal
clear >$(tty)

#output
echo -e
echo -e "       $b. $b        $w*$b Welcome Back $w$user !"
echo -e "      $b/#\ $b       $w*$b Cpu: $w$cpu /$b Cpu Temp: $w$((cputemp/1000))'C" 
echo -e "     $b/###\ $b      $w*$b Uptime: $w$updays days $w$uphours hours $w$upmins minutes $w$upsec seconds"
echo -e "    $b/#####\ $b     $w*$b Memory: $w$memory1 / $memory2 /$b available $w$memory3"
echo -e "   $b/##.-.##\ $b    $w*$b Kernel: $w$kernel"
echo -e "  $b/##(   )##\ $b   $w*$b Hostname: $w$hostname"
echo -e " $b/#.--   --.#\ $b  $w*$b Users logged in: $w`users | wc -w`"
echo -e "$b/'           '\ $b $w*$b Packages can be updated: $w$pacman"
echo -e